import React, {Component} from 'react';
import {View, TextInput, StyleSheet, TouchableOpacity, Text, Alert} from 'react-native';

class LoginForm extends Component {

	_onPressButton() {
		Alert.alert('Me livrei do SR!!!')
	}
	
	render() {
		return (
			<View style={styles.container}>
				<TextInput style={styles.input}
						   
						   autoCapitalize='none'
						   autoCorrect={false}
						   keyboardType='email-address'
						   placeholder='Email'
						   placeholderColor='rgba(255,255,255,0.7)'
				/>
				<TextInput style={styles.input}
						   placeholder='Pass'
						   
						   placeholderColor='rgba(255,255,255,0.7)'
						   secureTextEntry/>
				<TouchableOpacity style={styles.button} onPress={this._onPressButton}>
					<Text style={styles.textButton}> Login </Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({

	container: {
		padding: 20
	},

	input: {
		margin: 20,
		borderBottomColor: '#000000',
		borderBottomWidth: 1,
		height: 60,
		width: 300,
		backgroundColor: 'rgba(255,255,255,0.2)',
		padding: 10,
		color: '#fff'

	},

	button: {
		margin: 20,
		height: 70,
		width: 300,
		padding: 15,
		backgroundColor: '#F68B1F'
	},

	textButton: {
		color: '#0E2440',
		textAlign: 'center',
		fontWeight: '700',
		fontSize: 40

	}

});

export default LoginForm;