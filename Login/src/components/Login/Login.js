import React, {Component} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import LoginForm from './LoginForm';


export default class Login extends Component {
	render() {
		return (
			<View style={styles.container}>
				
				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/LOGO.png')}/>
				</View>

				<View style={styles.form}>
					<LoginForm/>
				</View>

			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
  	backgroundColor: '#1B2941',
  	flex: 1,
  },

  form: {
  	justifyContent: 'center', 
	alignItems: 'center',
	flexGrow: 1 
  },

  inner: {
    flexDirection: 'column',
	justifyContent: 'center', 
	alignItems: 'center',
	flexGrow: 1, 

  },

  image: {
  	width:  200, 
  	height: 200,
  },
});

